---
layout:     post
title:      jenkins,gitlab,oss脚本化
subtitle:    "\"\""
date:       2017-10-30
author:     Nicholas
header-img: img/post-bg-2015.jpg
catalog: true
tags:
    - 运维　
    - 自动化
---

## smile

# 自动化创建产品线

## gitlab创建项目(多个)

``` javascript

    # visibility_level：
    # 0　private
    # 10 internal
    # 20 public
    def create_project(self, name, description="this is a test project!", visibility_level=10):
        data = {
            'name': name,
            'description': description,
            'visibility_level': visibility_level
        }
        res = requests.post("http://gitlab.maxwell.com:18001/api/v3/projects",
                           data, headers=self.headers, verify=False)

        # return
```

＝＝＝

## jenkins创建project
#### 1. python jenkins
Python Jenkins is a python wrapper for the Jenkins REST API which aims to provide a more conventionally pythonic way of controlling a Jenkins server. It provides a higher-level API containing a number of convenience functions.

#### 2. 示例代码
```
import jenkins

server = jenkins.Jenkins('http://192.168.60.45:8080', username='*****', password='*')

# one:创建一个空配置文件的项目
server.create_job('empty', jenkins.EMPTY_CONFIG_XML)

# two:通过copy创建一个新项目
server.copy_job("a02-loc-be-lease-package","test-job")
```

## oss创建bucket
阿里提供了oss2相关api

#### 示例代码
```
AccessKeyId = '***********'
AccessKeySecret = '*************'
auth = oss2.Auth(AccessKeyId, AccessKeySecret)

# 创建bucket
# endpoint所在地区，例如杭州：　 http://oss-cn-hangzhou.aliyuncs.com
# bucket取名时条件比较苛刻，遵循以往的规则就行
bucket = oss2.Bucket(auth, endpoint, bucket-name)

# 私有读　oss2.BUCKET_ACL_PRIVATE
# 公共读　oss2.BUCKET_ACL_PUBLIC_READ
# 公共读写 oss2.BUCKET_ACL_PUBLIC_READ_WRITE
bucket.create_bucket("通过传入参数可控制bucket的权限")

```






                           


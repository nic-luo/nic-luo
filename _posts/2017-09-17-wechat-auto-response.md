---
layout:     post
title:      运用Python 接入聊天机器人做自动回复
subtitle:    "\"Hello Nic\""
date:       2017-09-28
author:     Nicholas Luo
header-img: img/post-bg-2015.jpg
catalog: true
tags:
    - 自动回复　聊天机器人　运维微信告警
---

## 一　python 安装微信第三方库  itchat

	> pip install itchat

## 事例代码
	
	#!/usr/bin/python
	# -*- coding: UTF-8 -*-

	import itchat

	@itchat.msg_register(itchat.content.TEXT)
	def text_reply(msg):
	    itchat.send(msg['Text'], msg['FromUserName'])
	    
	itchat.auto_login() #扫描QR Code 实现登陆

	itchat.run()

#### 如遇乱码
	引入sys模块
	
	reload(sys)
	sys.setdefaultencoding('utf-8')


---
## 二　接入图灵机器人实现聊天
	1.  [注册帐号](http://www.tuling123.com/) 

---
## 三　人机交互事例代码
	#!/usr/bin/python
	# -*- coding: UTF-8 -*-

	import json
	import requests
	import urllib
	import urllib2

	KEY = '************************************'    # change to your API KEY
	url = 'http://www.tuling123.com/openapi/api'

	req_info = u'call me cjz'.encode('utf-8')

	query = {'key': KEY, 'info': req_info}
	headers = {'Content-type': 'text/html', 'charset': 'utf-8'}

	# 方法一、用requests模块已get方式获取内容
	r = requests.get(url, params=query, headers=headers)
	res = r.text
	print json.loads(res).get('text').replace('<br>', '\n')

	# 方法二、用urllib和urllib2库获取内容
	data = urllib.urlencode(query)
	req = urllib2.Request(url, data)
	f = urllib2.urlopen(req).read()
	print json.loads(f).get('text').replace('<br>', '\n')
	
## 四　整合
	于是人机交互你就玩的飞起了。

---
**判断一个人的人品, 不是看他好起来做什么好事, 而是看他坏起来【不做】什么坏事。**
	
















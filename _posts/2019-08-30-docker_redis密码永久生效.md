---
layout:     post
title:      docker redis 密码永久生效
subtitle:    "\"运维血泪史\""
date:       2019-08-30
author:     Nicholas Luo
header-img: img/post-bg-2015.jpg
catalog: true
tags:
    - 运维
---

### 拉取镜像
	docker pull redis:5.0

### 启动并设置密码、开启持久化
	docker run -d --name redis-server -p 6379:6379 redis:5.0 --requirepass "mypassword" --appendonly yes
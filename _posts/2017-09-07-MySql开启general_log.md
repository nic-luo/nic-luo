---
layout:     post
title:      MySql 开启general_log
subtitle:    "\"运维血泪史\""
date:       2017-09-07
author:     Nicholas Luo
header-img: img/post-bg-2015.jpg
catalog: true
tags:
    - MySql
---

## 方法一

#### 设置日志产生的路径
    > set global general_log_file='/tmp/general.log';

#### 开启日志
    > set global general_log=on;

#### 关闭日志
    > set global general_log=off;
    
====

## 方法二

####　在配置文件中设置(my.cnf　重启生效)

    > general_log=1
    > general_log_file=&rsquo;/tmp/general.log&rsquo;;
  
====

    **有些人总喜欢不带我去喝咖啡，认为喝咖啡高雅，吃大蒜俗，还好，没有俗哪来的雅呢？**